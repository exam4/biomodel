\documentclass[main.tex]{subfiles}
\begin{document}
\section{Лекция 5. Структурный анализ метаболических сетей}
\textit{Метаболическая сеть} -- все возможные реакции обмена веществ в организме \\
\textit{Стехиометрическая матрица} (состоит из стехиометрических коэффициентов) описывает соотношения веществ в реакциях. \\
Пример: $ S_1 + S_2 \rightleftharpoons 2P \Rightarrow $ коэффициенты $\begin{pmatrix} -1 & -1 & 2 \end{pmatrix}$ (для прямой реакции). Записываются с точностью до множителя: может быть $\begin{pmatrix} -0.5 & -0.5 & 1 \end{pmatrix}$ и так далее.

Зная стехиометрические коэффициенты, можно написать уравнения баланса для реакции:
\[
	\frac{dS_1}{dt} = -v, \thickspace
	\frac{dS_2}{dt} = -v, \thickspace
	\frac{dP}{dt} = 2v
\]

Как правило, мы не будем писать в структурном анализе, чему равны скорости реакций (не рассматриваем модели Михаэлиса-Ментен и т. д)., лишь выводим какую-то информацию из самих уравнений реакции.

Пусть есть $m$ метаболитов и $r$ реакций $\Rightarrow$
$$ \frac{dS_i}{dt} = \sum_{j=1}^{r} n_{ij} v_j, \thickspace i = 1, ..., m $$

Стехиометрическая матрица $N_{m \times r} = \{n_{ij}\}_{j=1...r}^{i=1...m}$

Замечания:
\begin{itemize}
	\item Направление реакций, напомним, сверху вниз и слева направо (если не оговорено обратное)
	\item $v_{\text{необратим}} > 0$, $ v_{\text{обратим}} = v_+ + v_- \lessgtr 0 $ (скорость обратимой реакции может иметь любой знак)
\end{itemize}

Примеры:

\begin{tabular}{m{0.45\linewidth} | m{0.45\linewidth}}
	$ S_1 + 2 S_2 + 3 S_4 \xrightleftharpoons{v_1} S_3 + 2 S_5 $ &
	$ N =
	\begin{pmatrix}
		-1 \\
		-2 \\
		\phm 1 \\
		-3 \\
		\phm 2
	\end{pmatrix} $ \\
	\hline \vspace{5pt}
	\includegraphics[width=0.9\linewidth]{lec5/scheme1} &
	$ N = \begin{pmatrix}
		1 &      -1 &  \phm 0 &     -1 \\
		0 &  \phm 3 &      -1 & \phm 0 \\
		0 &  \phm 0 &  \phm 0 & \phm 2
	\end{pmatrix} $ \\
	\hline \vspace{5pt}
	\includegraphics[width=0.9\linewidth]{lec5/scheme2} &
	$ N = \begin{pmatrix}
		1 &      -1 &      -1 \\
		0 &  \phm 2 &      -1 \\
		0 &      -1 &  \phm 4
	\end{pmatrix} $ \\
\end{tabular}

$$ \frac{d\vec{S}}{dt} = N\vec{v}, \thickspace
\vec{S} = \begin{pmatrix}
	s_1 \\
	\vdots \\
	s_m
\end{pmatrix}, \thickspace
\vec{v} =  \begin{pmatrix}
	v_1 \\
	\vdots \\
	v_r
\end{pmatrix}
 $$

В большинстве случаев метаболические системы рассматриваются в равновесии.
$$ \vec{J} \vcentcolon= \vec{v}|_{\text{равн}} \text{ -- \textit{вектор равновесных потоков}} $$

Каким может быть$\vec{J}$? Возможны разные решения, и они соответствуют разным режимам функционирования биологической системы.

Структурная информация, заложенная в матрице $N$:
\begin{itemize}
	\item Пространство допустимых равновесных потоков $J$
	\item Закон сохранения:
		$$ \frac{d\vec{S}}{dt} = N \vec{v} = 0 $$
		Нетривиальные решения существуют, если $rank \space N < r$
\end{itemize}

Обозначим $K \vcentcolon= (\vec{k}_1, ..., \vec{k}_{rank(N) - r})$ -- базисные векторы ядра матрицы $N$.

$$ \vec{J} = \sum_{i=1}^{rank(N)-2} \alpha_i \vec{k}_i $$

Примеры:

\begin{tabular}{m{0.45\linewidth} | m{0.45\linewidth}}
	$ \xrightarrow{v_1} S_1 \xrightarrow{v_2} S_2 \xrightarrow{v_3} S_3 \xrightarrow{v_4} S_4  \xrightarrow{v_5} $ & \makecell{
		$ N = \begin{pmatrix}
			1 &      -1 &  \phm 0 & \phm 0 & \phm 0 \\
			0 &  \phm 1 &      -1 & \phm 0 & \phm 0 \\
			0 &  \phm 0 &  \phm 1 &     -1 & \phm 0 \\
			0 &  \phm 0 &  \phm 0 & \phm 1 &     -1
		\end{pmatrix} $ \\
		$r=5, \thickspace rank(N)=4 $ \\
		$dim(Ker(N))=5-4=1$ \\
		$J_i = J_j $ $ \forall i,j = 1...r$	} \\
	\hline \vspace{5pt}
	\includegraphics[width=0.9\linewidth]{lec5/scheme3} & \vspace{5pt} \makecell{
	$ N = \begin{pmatrix} 1 & -1 & -1 \end{pmatrix} $ \\
	$ r=3, \thickspace rank(N)=1, \thickspace dim(Ker(N))=2 $ \\
	$ \vec{k}_1 = \begin{pmatrix} 1 \\ 1 \\ 0 \end{pmatrix}, \vec{k}_2 = \begin{pmatrix} 1 \\ 0 \\ 1 \end{pmatrix}$ \\
	$ \vec{J}=\alpha_1 \vec{k}_1 + \alpha_2 \vec{k}_2 $ -- общее решение \\
	$ J_3 = \alpha_2, J_2 = \alpha_1, J_1 = \alpha_1 + \alpha_2 = J_2 + J_3 $ } \\
	\hline \vspace{5pt}
	\includegraphics[width=0.9\linewidth]{lec5/scheme4_cascade} & \vspace{5pt} \makecell{
		$S_1, S_3$ могут служить эффекторами \\
		 (<<сигнальный каскад>>) \\
		$ N = \begin{pmatrix}
			1 &      -1 &  \phm 0 & \phm 0 & \phm 0 \\
			0 &  \phm 0 &      -1 & \phm 1 & \phm 0 \\
			0 &  \phm 0 &  \phm 1 &     -1 & \phm 0 \\
			0 &  \phm 0 &  \phm 0 & \phm 0 & \phm 1
		\end{pmatrix} $ \\
	$ r=5, \thickspace rank(N)=3, \thickspace dim(ker(N)) = 2 $ \\
	$ \vec{k}_1 = \begin{pmatrix} 1 \\ 1 \\ 0 \\ 0 \\ 0 \end{pmatrix}, \vec{k}_2 = \begin{pmatrix} 0 \\ 0 \\ 1 \\ 1 \\ 0 \end{pmatrix}$ \\
	$ \vec{J}=\alpha_1 \vec{k}_1 + \alpha_2 \vec{k}_2 $ \\
	Т. о. $ J_1 = J_2, J_3 = J_4, J_5 = 0 $ } \\

    \hline \vspace{5pt}
    \includegraphics[width=0.9\linewidth]{lec5/scheme5} & \vspace{5pt} \makecell{
        $ \vec{k}_1 = \begin{pmatrix} 1 \\ 1 \\ 1 \\ 0 \\ 0 \\ -1 \end{pmatrix}, \vec{k}_2 = \begin{pmatrix} 1 \\ 0 \\ 0 \\ 0 \\ 1 \\ 0 \end{pmatrix}, \vec{k}_3 = \begin{pmatrix} -1 \\ -1 \\ -1 \\ -1 \\ 0 \\ 0 \end{pmatrix} $
    }
\end{tabular}

В равновесных системах $J_i = 0$; $J_s = J_l$, если $k_i^s=k_i^l \thickspace \forall i$ (\textit{цепочка неразветвлённых реакций})

\subsection{Элементарные потоковые моды}
(= простейшие режимы для потоков -- elementary flux models)

\textit{Metabolic Engineering}: анализируя метаболическую сеть,  мы можем
\begin{itemize}
    \item предложить модификацию / расширение сети за счёт добавления новых реакций с возможностью синтеза новых продуктов
    \item перенаправить потоки через заданные реакции
    \item изменить контроль за потоком в сети, чтобы, убыстряя или замедляя реакции, достигнуть нужного баланса
\end{itemize}
\textit{Потоковая мода} -- распределение равновесных потоков по реакциям в сети (решение стационарных уравнений = уравнений баланса). \\
\textit{Элементарная потоковая мода} -- та мода, которая не может быть разложена на более простые. \\
В реальных сетях бывает, что число метаболитов больше числа реакций, и в таких случаях решение не единственное.

Каждая реакция в метаболической сети определяется своим ферментом (иногда и несколькими ферментами). \\
Находя все элементарные потоковые моды, мы отвечаем на вопрос: сколько ферментов достаточно для поддержания равновесной сети? Иначе, решаем проблему оптимизации. \\
Потоковые моды можно формулировать в терминах алгебраических, а можно геометрически, рисуя диаграммы потоковых мод. Важно анализировать потоковые моды отдельно от записи равновесных потоков: помимо стехиометрической матрицы, у нас есть реакции, часть из которых обратима и часть необратима (в последнем случае есть ограничение на знак). Информация об ограничениях на знак не присутствует в стехиометрической матрице! \\
\textit{Путь} (pathway) -- набор реакций, соединяющих интересующие нас метаболиты. Путь может быть не единственный. Анализируя пути, мы можем сказать, какие в сети моды, какие элементарные моды. Пример: если на пути от $S_1$ к $S_n$ есть необратимые реакции, мы не можем построить путь из $S_n$ в $S_1$.
\begin{figure}[H]
    \centering \includegraphics[width=\myPictWidth]{lec5/pathway}
\end{figure}

Рассмотрим путь из четырёх обратимых реакций:

\begin{tabular}{m{0.45\linewidth} | m{0.45\linewidth}}
    \includegraphics[width=0.9\linewidth]{lec5/pathway1} & \makecell{
        $ \vec{J} = \begin{pmatrix} x \\ x \\ y \\ y \end{pmatrix} $, \\
        $x$, $y$ --  любые вещественные числа
    } \\
\end{tabular}

Здесь можно выд две элементарные потоковые моды: первая -- реакции 1 и 2 идут ($ x \ne 0 $), 3 и 4 нет ($ y=0 $); вторая -- $ x=0, \thickspace y \ne 0 $

\begin{figure}[H]
    \centering \includegraphics[width=\myPictWidth]{lec5/modes}
\end{figure}

$$ \vec{J} = \lambda_1 \vec{e}_1 + \lambda_2 \vec{e}_2, \thickspace \vec{e}_1 =
\begin{pmatrix} 1 \\ 1 \\ 0 \\ 0 \end{pmatrix}, \thickspace \vec{e}_2 = \begin{pmatrix} 0 \\ 0 \\ 1 \\ 1 \end{pmatrix} $$

Нам удалось расщепить систему на независимые реакции. В общем случае это, как правило, недостижимо. \\
Можно формализовать так: потоковая мода $ M = \{\vec{v} \in \mathds{R}^r : \vec{v} = \lambda \vec{J}^*, \lambda > 0\} $, причём выполнены условия
\begin{enumerate}[noitemsep]
    \item $ \vec{J}^* \ne 0 $
    \item $ \vec{J}^* \in ker(N) $
    \item $ \vec{J}^* $ удовлетворяет ограничениям на обратимость/необратимость реакции ($ v_i \overset{необратим.}> 0 $)
\end{enumerate}
Определение: $M$ обратимая $ \Leftrightarrow ( \vec{v} \in M \Rightarrow \{-\vec{v}\}$  -- тоже потоковая мода$)$. $M'\vcentcolon= \{-\vec{v}\} $ называют обратной потоковой модой. \\

Примеры: рассмотрим две простые метаболические системы
\begin{figure}[H]
    \centering \includegraphics[width=\myPictWidth]{lec5/modes_examples}
\end{figure}

Второй случай отличается от первого только обратимостью реакции 2. Стехиометрические матрицы одинаковые, ядро матрицы, определяющее пространство потоков, одно и то же; но потоковые моды разные. Выведем их. \\
В первом примере метаболиты $S_1, S_2$ -- \textit{внутренние} метаболиты; предположим, что  $S_3, S_4, S_5$ -- \textit{внешние} метаболиты. Внешние -- те, которые в стационарном состоянии не меняются, они виртуально связаны с <<бесконечными резервуарами>>: $S_1$, если он является источником, можно бесконечно черпать (он находится в абсолютном избытке); $S_4$, если он является стоком для $S_3$, можно неограниченно <<сливать>>. Говорят, что $S_2, S_3$ зажаты (clamped).

Все кратчайшие пути, соединяющие внешние метаболиты, являются элементарными потоковыми модами. Вот они для примера (А):
\begin{figure}[H]
    \centering \includegraphics[width=\myPictWidth]{lec5/modes_elementary_a}
\end{figure}
Все эти элементарные моды могут существовать в стационарном состоянии. \\
Для примера (Б) моды почти такие же, только отсутствует правая верхняя и левая нижняя мода, поскольку реакция 2 необратимая. \\
Те же моды можно вывести алгебраически, записав базисные вектора ядра стехиометрической матрицы. Но для двух внутренних метаболитов будет только два базисных вектора:
$$ N =
\begin{pmatrix}
    0 &     -1 & \phm 0 & -1 \\
    0 & \phm 1 &     -1 & \phm 0 \end{pmatrix}, \thickspace
dim(ker(N))=2, \thickspace \vec{k}_1 = \begin{pmatrix} 1 \\ 0 \\ 0 \\ 0 \end{pmatrix}, \thickspace \vec{k}_2 = \begin{pmatrix} 1 \\ 1 \\ 1 \\ 0 \end{pmatrix}
 $$
Нужно получить 6 потоковых мод комбинацией двух базисных векторов. Можно показать, что будут получаться потоки
$$ \vec{v} =
\begin{pmatrix} 1 \\ 1 \\ 1 \\ 0 \end{pmatrix},
\begin{pmatrix} 1 \\ 0 \\ 0 \\ 1 \end{pmatrix},
\begin{pmatrix} \phm 0 \\ -1 \\ -1 \\ \phm 1 \end{pmatrix},
\begin{pmatrix} -1 \\ \phm 0 \\ \phm 0 \\ -1 \end{pmatrix},
\begin{pmatrix} \phm 0 \\ \phm 1 \\ \phm 1 \\ -1 \end{pmatrix} $$

Резюме: биологический смысл элементарных потоковых мод -- нахождение минимального набора ферментов для поддержания сети. Число э. п. м. может быть $\ge$ числа базисных векторов стехиометрической матрицы. Применение анализа потоковых мод -- осуществление контроля за поведением метаболической сети. Для более сложных сетей есть алгоритмы нахождения и анализа ЭПМ, реализованные в программных пакетах:
\begin{itemize}[noitemsep]
    \item METATOOL (часть Cobra MATLAB toolbox)
    \item более новый, 2015 года -- tEFMA (thermodynamic elementary flux mode analysis tool)
\end{itemize}

Пример \href{https://docviewer.yandex.ru/view/432429588/?page=1&*=V%2B7U1GEDggX%2FSRvQQsK%2F6EYRxj17InVybCI6InlhLWRpc2stcHVibGljOi8vU3dFYTVraDRBbFBUUDJpVUp3bW1USUxEVGJlU21wVC9WdXBTQVduTjREND06L3BkZi5zY2llbmNlZGlyZWN0YXNzZXRzLmNvbSAwNy4xMC4yMDIwLCAxOV81M18xNy5wZGYiLCJ0aXRsZSI6InBkZi5zY2llbmNlZGlyZWN0YXNzZXRzLmNvbSAwNy4xMC4yMDIwLCAxOV81M18xNy5wZGYiLCJub2lmcmFtZSI6ZmFsc2UsInVpZCI6IjQzMjQyOTU4OCIsInRzIjoxNjA4OTI1NTYxMDc2LCJ5dSI6IjM0MzAzMDEwNjE1ODQ4ODI3NzEifQ%3D%3D}{(см. статью "Detection of elementary flux modes in biochemical net\-works..." на Яндекс Диске Виталия Валерьевича)}:
 метаболическая сеть бактерии \textit{Esche\-richia Coli}. Оранжевые кружочки -- внутренние метаболиты, жёлтые -- внешние.  Внешние -- например, кислород; кружок -- цикл Кребса (важная часть дыхательной системы клетки). Цикл Кребса может существовать в разных режимах, соответствующих разным условиям, в которых находится бактерия.
 В этом цикле выделяются 16 потоковых мод (в частности, простейшие, наподобие превращений АТФ в АДФ; есть и более сложные). Почти каждая из этих шестнадцати -- некий отдельный простейший режим функционирования цикла Кребса. Видим здесь некоторую пластичность метаболической сети и видим, насколько различные получаются ферменты и пути.

\begin{figure}[H]
    \centering \includegraphics[width=\myPictWidth]{lec5/ecoli_metabolic}
    \captionsetup{labelformat=empty}
    \caption{Метаболические пути бактерии \textit{Escherichia Coli}}
\end{figure}



\end{document}